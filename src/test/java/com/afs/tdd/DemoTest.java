package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class DemoTest {
    @Test
    void write_your_first_test() {

    }

    @Test
    void should_change_to_0_1_N_when_executeCommand_given_location_0_0_N_and_command_move(){
        //given
        Location initialLocation = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.MOVE;

        //when
        Location executeCommandLocation = marsRover.executeCommand(command);

        //then

        Assertions.assertEquals(0,executeCommandLocation.getCoordinateX());
        Assertions.assertEquals(1,executeCommandLocation.getCoordinateY());
        Assertions.assertEquals(Direction.NORTH,executeCommandLocation.getDirection());
    }


    @Test
    void should_change_to_1_0_E_when_executeCommand_given_location_0_0_E_and_command_move(){
        //given
        Location initialLocation = new Location(0, 0, Direction.EAST);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.MOVE;

        //when
        Location executeCommandLocation = marsRover.executeCommand(command);

        //then

        Assertions.assertEquals(1,executeCommandLocation.getCoordinateX());
        Assertions.assertEquals(0,executeCommandLocation.getCoordinateY());
        Assertions.assertEquals(Direction.EAST,executeCommandLocation.getDirection());
    }


    @Test
    void should_change_to_0_minusOne_S_when_executeCommand_given_location_0_0_S_and_command_move(){
        //given
        Location initialLocation = new Location(0, 0, Direction.SOUTH);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.MOVE;

        //when
        Location executeCommandLocation = marsRover.executeCommand(command);

        //then

        Assertions.assertEquals(0,executeCommandLocation.getCoordinateX());
        Assertions.assertEquals(-1,executeCommandLocation.getCoordinateY());
        Assertions.assertEquals(Direction.SOUTH,executeCommandLocation.getDirection());
    }

    @Test
    void should_change_to_minusOne_0_W_when_executeCommand_given_location_0_0_W_and_command_move(){
        //given
        Location initialLocation = new Location(0, 0, Direction.WEST);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.MOVE;

        //when
        Location executeCommandLocation = marsRover.executeCommand(command);

        //then

        Assertions.assertEquals(-1,executeCommandLocation.getCoordinateX());
        Assertions.assertEquals(0,executeCommandLocation.getCoordinateY());
        Assertions.assertEquals(Direction.WEST,executeCommandLocation.getDirection());
    }

    @Test
    void should_change_to_0_0_W_when_executeCommand_given_location_0_0_N_and_command_turnLeft(){
        //given
        Location initialLocation = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.TURN_LEFT;

        //when
        Location executeCommandLocation = marsRover.executeCommand(command);

        //then

        Assertions.assertEquals(0,executeCommandLocation.getCoordinateX());
        Assertions.assertEquals(0,executeCommandLocation.getCoordinateY());
        Assertions.assertEquals(Direction.WEST,executeCommandLocation.getDirection());
    }

    @Test
    void should_change_to_0_0_S_when_executeCommand_given_location_0_0_W_and_command_turnLeft(){
        //given
        Location initialLocation = new Location(0, 0, Direction.WEST);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.TURN_LEFT;

        //when
        Location executeCommandLocation = marsRover.executeCommand(command);

        //then

        Assertions.assertEquals(0,executeCommandLocation.getCoordinateX());
        Assertions.assertEquals(0,executeCommandLocation.getCoordinateY());
        Assertions.assertEquals(Direction.SOUTH,executeCommandLocation.getDirection());
    }

    @Test
    void should_change_to_0_0_E_when_executeCommand_given_location_0_0_S_and_command_turnLeft(){
        //given
        Location initialLocation = new Location(0, 0, Direction.SOUTH);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.TURN_LEFT;

        //when
        Location executeCommandLocation = marsRover.executeCommand(command);

        //then

        Assertions.assertEquals(0,executeCommandLocation.getCoordinateX());
        Assertions.assertEquals(0,executeCommandLocation.getCoordinateY());
        Assertions.assertEquals(Direction.EAST,executeCommandLocation.getDirection());
    }

    @Test
    void should_change_to_0_0_N_when_executeCommand_given_location_0_0_E_and_command_turnLeft(){
        //given
        Location initialLocation = new Location(0, 0, Direction.EAST);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.TURN_LEFT;

        //when
        Location executeCommandLocation = marsRover.executeCommand(command);

        //then

        Assertions.assertEquals(0,executeCommandLocation.getCoordinateX());
        Assertions.assertEquals(0,executeCommandLocation.getCoordinateY());
        Assertions.assertEquals(Direction.NORTH,executeCommandLocation.getDirection());
    }

    @Test
    void should_change_to_0_0_E_when_executeCommand_given_location_0_0_N_and_command_turnRight(){
        //given
        Location initialLocation = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.TURN_RIGHT;

        //when
        Location executeCommandLocation = marsRover.executeCommand(command);

        //then

        Assertions.assertEquals(0,executeCommandLocation.getCoordinateX());
        Assertions.assertEquals(0,executeCommandLocation.getCoordinateY());
        Assertions.assertEquals(Direction.EAST,executeCommandLocation.getDirection());
    }

    @Test
    void should_change_to_0_0_S_when_executeCommand_given_location_0_0_E_and_command_turnRight(){
        //given
        Location initialLocation = new Location(0, 0, Direction.EAST);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.TURN_RIGHT;

        //when
        Location executeCommandLocation = marsRover.executeCommand(command);

        //then

        Assertions.assertEquals(0,executeCommandLocation.getCoordinateX());
        Assertions.assertEquals(0,executeCommandLocation.getCoordinateY());
        Assertions.assertEquals(Direction.SOUTH,executeCommandLocation.getDirection());
    }

    @Test
    void should_change_to_0_0_W_when_executeCommand_given_location_0_0_S_and_command_turnRight(){
        //given
        Location initialLocation = new Location(0, 0, Direction.SOUTH);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.TURN_RIGHT;

        //when
        Location executeCommandLocation = marsRover.executeCommand(command);

        //then

        Assertions.assertEquals(0,executeCommandLocation.getCoordinateX());
        Assertions.assertEquals(0,executeCommandLocation.getCoordinateY());
        Assertions.assertEquals(Direction.WEST,executeCommandLocation.getDirection());
    }

    @Test
    void should_change_to_0_0_N_when_executeCommand_given_location_0_0_W_and_command_turnRight(){
        //given
        Location initialLocation = new Location(0, 0, Direction.WEST);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.TURN_RIGHT;

        //when
        Location executeCommandLocation = marsRover.executeCommand(command);

        //then

        Assertions.assertEquals(0,executeCommandLocation.getCoordinateX());
        Assertions.assertEquals(0,executeCommandLocation.getCoordinateY());
        Assertions.assertEquals(Direction.NORTH,executeCommandLocation.getDirection());
    }


    @Test
    void should_change_to_0_1_N_when_executeCommand_given_location_0_0_N_and_execute_multiple_command(){
        //given
        Location initialLocation = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(initialLocation);
        List<Command> commands = new ArrayList<>();
        commands.add(Command.MOVE);
        commands.add(Command.TURN_LEFT);
        commands.add(Command.TURN_RIGHT);

        //when
        Location executeCommandLocation = marsRover.executeMultipleCommands(commands);

        //then

        Assertions.assertEquals(0,executeCommandLocation.getCoordinateX());
        Assertions.assertEquals(1,executeCommandLocation.getCoordinateY());
        Assertions.assertEquals(Direction.NORTH,executeCommandLocation.getDirection());
    }
}
