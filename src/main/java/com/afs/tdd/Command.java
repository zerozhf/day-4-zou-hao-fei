package com.afs.tdd;

public enum Command {
    TURN_RIGHT,
    TURN_LEFT,
    MOVE,
}
