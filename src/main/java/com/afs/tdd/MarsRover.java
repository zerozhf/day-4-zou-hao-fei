package com.afs.tdd;

import java.util.Arrays;
import java.util.List;

public class MarsRover {
    private Location location;


    public MarsRover(Location location) {
        this.location = location;
    }


//    public Location executeCommand(Command command){
//        if(command == Command.MOVE){
//            if(location.getDirection() == Direction.NORTH){
//                location.setCoordinateY(location.getCoordinateY()+1);
//                return location;
//            }else if (location.getDirection() == Direction.EAST){
//                location.setCoordinateX(location.getCoordinateX()+1);
//                return location;
//            }else if (location.getDirection() == Direction.SOUTH){
//                location.setCoordinateY(location.getCoordinateY()-1);
//                return location;
//            }else {
//                location.setCoordinateX(location.getCoordinateX()-1);
//                return location;
//            }
//        }else if (command == Command.TURN_LEFT){
//            if(location.getDirection() == Direction.NORTH){
//                location.setDirection(Direction.WEST);
//                return location;
//            }else if (location.getDirection() == Direction.WEST){
//                location.setDirection(Direction.SOUTH);
//                return location;
//            }else if (location.getDirection() == Direction.SOUTH){
//                location.setDirection(Direction.EAST);
//                return location;
//            }else {
//                location.setDirection(Direction.NORTH);
//                return location;
//            }
//        }else{
//            if(location.getDirection() == Direction.NORTH){
//                location.setDirection(Direction.EAST);
//                return location;
//            }else if (location.getDirection() == Direction.EAST){
//                location.setDirection(Direction.SOUTH);
//                return location;
//            }else if (location.getDirection() == Direction.SOUTH){
//                location.setDirection(Direction.WEST);
//                return location;
//            }else {
//                location.setDirection(Direction.NORTH);
//                return location;
//            }
//        }
//    }


    public Location executeCommand(Command command){
        if(command == Command.MOVE){
            switch (location.getDirection()){
                case NORTH: location.setCoordinateY(location.getCoordinateY()+1); return location;
                case WEST: location.setCoordinateX(location.getCoordinateX()-1); return location;
                case SOUTH: location.setCoordinateY(location.getCoordinateY()-1); return location;
                default: location.setCoordinateX(location.getCoordinateX()+1); return location;
                }
        }else if (command == Command.TURN_LEFT){
            switch (location.getDirection()){
                case NORTH: location.setDirection(Direction.WEST); return location;
                case WEST: location.setDirection(Direction.SOUTH); return location;
                case SOUTH: location.setDirection(Direction.EAST); return location;
                default: location.setDirection(Direction.NORTH); return location;
            }
        }else{
            switch (location.getDirection()){
                case NORTH: location.setDirection(Direction.EAST); return location;
                case WEST: location.setDirection(Direction.NORTH); return location;
                case SOUTH: location.setDirection(Direction.WEST); return location;
                default: location.setDirection(Direction.SOUTH); return location;
            }
        }

    }

   public Location executeMultipleCommands(List<Command> commands){
        if(commands == null){
            return location;
        }

       for (Command command : commands) {
           location = executeCommand(command);
       }

       return location;
   }


}
