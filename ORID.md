 # **Daily Report (2023/07/12)**  
## O

### 1. Code review
  

* Several of the test cases for yesterday's assignment did not pass. Today, in the code review section. I found bugs in the code and all the test cases passed.
* I learned the Observer pattern from the code of other students.

### TDD

1. Write a test that fails.
2. Make the code work.
3. Eliminate redundancy.

* Simplify design
*  Test as documentation
*  Quick feedback 
*  Safety net

## R
meaningful
## I
* I think code review is very useful.I still have some doubts about TDD.
* The most meaaningful is TDD.
## D
I will start using TDD in actual projects.

